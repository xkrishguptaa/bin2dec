<div align="center">
  <img src="https://gitlab.com/kkrishguptaa-learns/bin2dec/-/raw/main/assets/logo.png" height="100px" width="100px" />
  <br />
  <h1>Bin2Dec</h1>
  <p>Simply Beautiful Binary to Decimal Converter</p>
  <p><a href="https://kkrishguptaa-learns.gitlab.io/bin2dec"><img src="https://img.shields.io/badge/View%20Deployed-2965F1?style=for-the-badge" alt="View Deployed" /></a></p>
</div>

## 📸 Screenshots

| Desktop | Mobile |
| --- | --- |
| ![Screenshot of the application, the page includes a heading "Binary To Decimal", a textbox for input and a block of output](https://gitlab.com/kkrishguptaa-learns/bin2dec/-/raw/main/assets/screenshots/desktop.png) |   ![Screenshot of Bin2Dec On A Mobile Device To Showcase That It's Responsive](https://gitlab.com/kkrishguptaa-learns/bin2dec/-/raw/main/assets/screenshots/mobile.png)  |

## 💡 Origin

Details can be found => [#1](https://github.com/kkrishguptaa/learning/issues/1)
